#ifndef ADDER_H
#define ADDER_H

#include <QDialog>
#include <QVector>

namespace Ui {
class Adder;
}

class Adder : public QDialog
{
    Q_OBJECT

public:
    explicit Adder(QWidget *parent = 0);
    ~Adder();


public slots:
    void updateLCD(int lcd, int num);
    void shiftLeftForValue(int num);
    void conections();
    void btnMasAction();
    void btnMenosAction();
    void clearAllLCD();

private slots:
    void on_btnUno_clicked();
    void on_btnCero_clicked();

    int lcdToInt();
    void intToLCD(int number);

    void on_btnIgual_clicked();

private:
    Ui::Adder *ui; /* UI object */
    QVector<int> *nList_;
    int acc_; /* Accumulator */
    char op_; /* Operator */
};

#endif // ADDER_H
