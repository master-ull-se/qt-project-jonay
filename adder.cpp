#include "adder.h"
#include "ui_adder.h"
#include <stack>

Adder::Adder(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Adder)
{
    ui->setupUi(this);
    op_ = '=';
}

Adder::~Adder()
{
    delete ui;
}

/* Connect button signal to appropriate slot */
void Adder::conections()
{
    connect(ui->btnMas, SIGNAL (released()), this, SLOT (btnMasAction()));
    connect(ui->btnMenos, SIGNAL (released()), this, SLOT (btnMenosAction()));
}

/* Insert a LCD number in a specific position */
void Adder::updateLCD(int lcd, int num)
{
    switch(lcd)
    {
        case 0:
            ui->lcd0->display(num);
            break;
        case 1:
            ui->lcd1->display(num);
            break;
        case 2:
            ui->lcd2->display(num);
            break;
        case 3:
            ui->lcd3->display(num);
            break;
    }
}

void Adder::clearAllLCD() {
    updateLCD(0, 0);
    updateLCD(1, 0),
    updateLCD(2, 0);
    updateLCD(3, 0);
}

/* Insert a LCD number in the last position */
void Adder::shiftLeftForValue(int num)
{
    updateLCD(0, ui->lcd1->intValue());
    updateLCD(1, ui->lcd2->intValue());
    updateLCD(2, ui->lcd3->intValue());
    updateLCD(3, num);
}

/* Action: + button */
void Adder::btnMasAction()
{
    op_ = '+';
    acc_ = lcdToInt();
    clearAllLCD();
}


/* Action: - button */
void Adder::btnMenosAction()
{
    op_ = '-';
    acc_ = lcdToInt();
    clearAllLCD();
}

void Adder::on_btnUno_clicked()
{
    shiftLeftForValue(1);
}

void Adder::on_btnCero_clicked()
{
    shiftLeftForValue(0);
}

int Adder::lcdToInt() {
    int powerOfEight = ui->lcd0->intValue() * 8;
    int powerOfFour = ui->lcd1->intValue() * 4;
    int powerOfTwo = ui->lcd2->intValue() * 2;
    int powerOfOne = ui->lcd3->intValue();

    return powerOfEight + powerOfFour + powerOfTwo + powerOfOne;
}

void Adder::intToLCD(int number) {
    clearAllLCD();

    std::stack<int> result;
    while (number > 0) {
        result.push(number % 2);
        number = number / 2;
    }

    while (!result.empty()) {
        shiftLeftForValue(result.top());
        result.pop();
    }
}

void Adder::on_btnIgual_clicked()
{
    switch (op_) {
    case '=':
        // LCD content becomes acc
        acc_ = lcdToInt();
        break;
    case '+':
        acc_ = acc_ + lcdToInt();
        break;
    case '-':
        acc_ = acc_ - lcdToInt();
        break;

    default:
        break;
    }

    intToLCD(acc_);
    acc_ = 0;
    op_ = '=';
}
