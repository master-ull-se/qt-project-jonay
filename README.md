# Calculadora binaria en Qt

## Descripción

UI de calculadora binaria desarrollada en Qt para emular en Raspberry Pi.

## Licencia

* GPL v3

## Autores
* Andrés Nacimiento García
* David Dinesh Ashok Harjani Harjani