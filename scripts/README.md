# Scripts

#### 1qemu

Primer script para ejecutar, este scritp ejecuta la máquina virtual y configura el entorno. 

```
./1qemu
```

#### 2qtcreator

Ejecuta qtcreartor para ejecutar el resultado en la máquina virtual creada con qemux86.

```
./2qtcreator
```