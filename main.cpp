#include <QApplication> /* Base */
#include <QLCDNumber> /* LCD Numbers */
#include <QString> /* Strings */
#include "adder.h"

int main(int argc, char **argv) {
    QApplication app (argc, argv);

    Adder *adder = new Adder();
    adder->show();

    /* Testing UI LCD numbers */
    adder->conections();

    return app.exec();
}
